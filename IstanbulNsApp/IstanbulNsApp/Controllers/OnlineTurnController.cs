﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IstanbulNsApp.Filters;
using IstanbulNsApp.Libs;
using IstanbulNsApp.Repositories;
using IstanbulNsApp.Resources.DTO;
using IstanbulNsApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace IstanbulNsApp.Controllers
{
    [Validate]
    public class OnlineTurnController : Controller
    {
        private readonly IConfiguration _config;
        private readonly HttpClient _fc;
        private readonly IStringLocalizer<SharedResource> _localizer;
        public OnlineTurnController(IConfiguration config, IHttpClientFactory fc, IStringLocalizer<SharedResource> localizer)
        {
            _config = config;
            _fc = fc.CreateClient(name: "ApiRequests");
            _config = config;
            _localizer = localizer;
        }
        public async Task<IActionResult> Index([FromQuery] int doctor)
        {
            int langId = 1;

            if (Request.Cookies["LangKey"] != null)
            {
                langId = Convert.ToInt32(Request.Cookies["LangKey"]);
            }
            var services = await new ServiceNodeAsync<object, OnlineTurnDTO>(_fc).GetClientAsync("/api/online_query/info/" + doctor+"/"+langId);
            var model = new OnlineTurnViewModel();
            model.OnlineData = services.Data;
            return View(model);
        }
        public async Task<IActionResult> Confirm([FromForm] OnlineTurnViewModel request)
        {
            OnlineQueryDTO resources = new OnlineQueryDTO();

            string code = (new Random().Next(1000, 999999999)).ToString();
            resources.Code = code;
            resources.DoctorId = request.DoctorId;
            resources.ServiceId = request.ServiceId;
            string parsedTime = request.Date + " " + request.Hour;
            DateTime time = Convert.ToDateTime(parsedTime);
            resources.QueryDate = time;
            resources.ServeDate = DateTime.Now;
            resources.PhoneNumber = request.PhoneNumber;
            resources.Email = request.Email;
            resources.Info = request.Info;
            resources.Subject = request.Subject;
            var resp = await new ServiceNodeAsync<OnlineQueryDTO, object>(_localizer, _fc).PostClientAsync(resources, "/api/online_query/confirm");
            if (resp.IsCatched == 1)
            {
                TempData["ServerResponseError"] = resp.Message.ToString();
                return RedirectToAction("Index","OnlineTurn",new {doctor = request.DoctorId});
            }
            TempData["ServerResponseSuccess"] = code;
            return RedirectToAction("Index","Home");
        }
    }
}

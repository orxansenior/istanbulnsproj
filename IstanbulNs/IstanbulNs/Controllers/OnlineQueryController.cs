﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IstanbulNs.Data;
using IstanbulNs.Models;
using IstanbulNs.Repositories;
using IstanbulNs.Repositories.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IstanbulNs.Controllers
{
    [Route("api/online_query")]
    [ApiController]
    public class OnlineQueryController : ControllerBase
    {
        private readonly IndexContext _db;
        public OnlineQueryController(IndexContext db)
        {
            _db = db;
        }
        [HttpGet("info/{doctor}/{langId}")]
        public async Task<IActionResult> GetExternedInfo(int doctor, int langId)
        {
            #region FunctionBody
            var doctorSelected = await _db.Doctors.Where(a => a.Id == doctor).Include("DoctorsInfos").Select(
                a => new
                {
                    Id = a.Id,
                    Name = a.Name,
                    ServiceId = a.ServiceId,
                    DoctorInfo = a.DoctorsInfos.FirstOrDefault()
                }).FirstOrDefaultAsync();

            var service = (from services in _db.Services.AsParallel()
                           join name in _db.ServiceNames.AsParallel() on services.Id equals name.ServiceId
                           where services.Id == doctorSelected.ServiceId && name.LangId == langId
                           select new
                           {
                               services.Id,
                               name.Name
                           });
            return Ok(new ReturnMessage(data: new { doctorSelected = doctorSelected, service = service.FirstOrDefault() }));
            #endregion
        }
        [HttpPost("confirm")]
        public async Task<IActionResult> Confirm([FromBody] OnlineQuery request)
        {
            #region FunctionBody
            bool existed = await _db.OnlineQueries.AnyAsync(
                a =>
                a.QueryDate == request.QueryDate &&
                a.IsDeleted == 0 &&
                a.IsSchedule == 1);
            if (existed)
            {
                return BadRequest(new ReturnErrorMessage(errortype: (int)ErrorTypes.Errors.ExistedTime, message: "This Time is exist by other user"));
            }
            DoctorsInfo doctor = await _db.DoctorsInfos.FirstOrDefaultAsync(a => a.DoctorId == request.DoctorId);
            if (
                doctor.WorkTimeFromDate > (int)request.QueryDate.DayOfWeek ||
                doctor.WorkTimeToDate < (int)request.QueryDate.DayOfWeek 
               )

            {
                return BadRequest(new ReturnErrorMessage(errortype: (int)ErrorTypes.Errors.NotAllowedTime, message: "This Time not registered by Doctor"));
            }
            try
            {
                request.ServeDate = DateTime.Now;
                request.IsSchedule = 1;
                await _db.OnlineQueries.AddAsync(request);

            }
            catch (Exception x)
            {
                return BadRequest(new ReturnErrorMessage(errortype: (int)ErrorTypes.Errors.Internal, message: x.Message, code: 500));
            }
            await _db.SaveChangesAsync();
            return Ok(new ReturnMessage());
            #endregion
        }
        [HttpGet("/data")]
        public IActionResult GetData()
        {
            return Ok(TimeSpan.FromHours(8.5).ToString(@"hh\:mm"));

        }
        [HttpGet("/data1")]
        public IActionResult GetData1()
        {
            return Ok(TimeSpan.Parse(DateTime.Now.ToString("HH:mm")).TotalHours);
        }
    }
}
